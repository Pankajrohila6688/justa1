import React, {useEffect} from 'react';
import './App.css';
import Header from './components/Header';
import Banner from './components/Banner';
import Body from './components/Body';
import Footer from './components/Footer';
import Login from './components/Login';
import ListingForm from './components/forms/ListingForm';
import FreeplanForm from './components/forms/FreeplanForm';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { auth } from './components/firebase';
import { useStateValue } from './components/StateProvider';
import Travel from './components/catagories pages/Travel';
import Automotive from './components/catagories pages/Automotive';
import Medical from './components/catagories pages/Medical';
import Repairs from './components/catagories pages/Repairs';
import Shopping from './components/catagories pages/Shopping';
import Education from './components/catagories pages/Education';
import Restaurant from './components/catagories pages/Restaurant';
import HouseKeeping from './components/catagories pages/HouseKeeping';
import PersonalCare from './components/catagories pages/PersonalCare';
import Groceries from './components/catagories pages/Groceries';
import Wedding from './components/catagories pages/Wedding';
import Decor from './components/catagories pages/Decor';
import BabyCare from './components/catagories pages/BabyCare';
import BillsAndRecharge from './components/catagories pages/BillsAndRecharge';
import Hotels from './components/catagories pages/Hotels';
import Cinema from './components/catagories pages/Cinema';
import Tatto from './components/catagories pages/Tatto';
import Beauty from './components/catagories pages/Beauty';
import Security from './components/catagories pages/Security';
import PetCare from './components/catagories pages/PetCare';
import Packers from './components/catagories pages/Packers';
import Internet from './components/catagories pages/Internet';
import Insuarance from './components/catagories pages/Insuarance';
import Fitness from './components/catagories pages/Fitness';
import Music from './components/catagories pages/Music';
import Sports from './components/catagories pages/Sports';
import Advertise from './components/catagories pages/Advertise';
// import FormDetails from './components/forms/FormDetails';
import Signup from './components/Signup';
import ProductService from './components/product service/ProductService';
import Details from './components/dashboard/Details';
import DetailsEdit from './components/dashboard/DetailsEdit';
import ScrollToTop from './components/ScrollToTop';
import Login1 from './components/Login1';
import Signup1 from './components/Signup1';
import Dash from "./components/dashboard/Dash";
import DashBusiness from "./components/dashboard/DashBusiness";

// import { db } from './components/firebase';

function App() {
  const [{}, dispatch] = useStateValue();

  useEffect(() => {

    auth.onAuthStateChanged(authUser => {
      // console.log(authUser);
      // db.collection('loginform').add({
      //     displayName: authUser.displayName,
      //     email: authUser.email,
      // })

      if (authUser) {
        dispatch({
          type: 'SET_USER',
          user: authUser
        })
      } else {
        dispatch({
          type: 'SET_USER',
          user: null
        })
      }
    })
  }, [dispatch])

  return (
    <Router>
      <div className="app">
        <ScrollToTop />
        <Switch>
        <Route path="/Groceries">
            <Header />
            <Groceries />
          </Route>
          <Route path="/Wedding">
            <Header />
            <Wedding />
          </Route>
          <Route path="/BabyCare">
            <Header />
            <BabyCare />
          </Route>
          <Route path="/BillsAndRecharge">
            <Header />
            <BillsAndRecharge />
          </Route>
          <Route path="/Hotels">
            <Header />
            <Hotels />
          </Route>
          <Route path="/Cinema">
            <Header />
            <Cinema />
          </Route>
          <Route path="/Tatto">
            <Header />
            <Tatto />
          </Route>
          <Route path="/Beauty">
            <Header />
            <Beauty />
          </Route>
          <Route path="/Security">
            <Header />
            <Security />
          </Route>
          <Route path="/PetCare">
            <Header />
            <PetCare />
          </Route>
          <Route path="/Packers">
            <Header />
            <Packers />
          </Route>
          <Route path="/Internet">
            <Header />
            <Internet />
          </Route>
          <Route path="/HouseKeeping">
            <Header />
            <HouseKeeping />
          </Route>
          <Route path="/Insuarance">
            <Header />
            <Insuarance />
          </Route>
          <Route path="/Decor">
            <Header />
            <Decor />
          </Route>
          <Route path="/Fitness">
            <Header />
            <Fitness />
          </Route>
          <Route path="/Music">
            <Header />
            <Music />
          </Route>
          <Route path="/Advertise">
            <Header />
            <Advertise />
          </Route>
          <Route path="/Sports">
            <Header />
            <Sports />
          </Route>

          <Route path="/productService/:productServiceId">
            <Header />
            <ProductService />
          </Route>

          <Route path="/Travel">
            <Header />
            <Travel />
          </Route>

          <Route path="/Automotive">
            <Header />
            <Automotive />
          </Route>

          <Route path="/Medical">
            <Header />
            <Medical />
          </Route>

          <Route path="/Repairs">
            <Header />
            <Repairs />
          </Route>

          <Route path="/Shopping">
            <Header />
            <Shopping />
          </Route>

          <Route path="/Education">
            <Header />
            <Education />
          </Route>

          <Route path="/Restaurant">
            <Header />
            <Restaurant />
          </Route>

          <Route path="/Personalcare">
            <Header />
            <PersonalCare />
          </Route>

          <Route path="/Details">
            {/* <Header /> */}
            <Details />
          </Route>

          <Route path="/Login">
            {/* <Header /> */}
            <Login />
          </Route>
          <Route path="/login1">
            {/* <Header /> */}
            <Login1 />
          </Route>

          <Route path="/Signup">
            {/* <Header /> */}
            <Signup />
          </Route>
          <Route path="/signup1">
            {/* <Header /> */}
            <Signup1 />
          </Route>

          {/* <Route path="/formdetails">
            <FormDetails />
          </Route> */}
          
          <Route path="/freeplanForm">
            {/* <Header /> */}
            <FreeplanForm />
          </Route>

          <Route path="/listingform">
            <Header />
            <ListingForm />
          </Route>
          <Route path="/details">
            <Header />
            <Details />
            <Footer />
          </Route>
          <Route path="/detailsEdit">
            <Header />
            <DetailsEdit />
            <Footer />
          </Route>
          
          <Route path="/dash">
            <Header />
            <Dash />
          </Route>
          <Route path="/dashBusiness">
            <Header />
            <DashBusiness />
          </Route>


          <Route path="/">
            <Banner />
            <Body />
          </Route>

        </Switch>
      </div>
    </Router>
  );
} 

export default App;
