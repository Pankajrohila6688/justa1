import React from "react";
import './Reviews.css';
import ThumbUpAltRoundedIcon from '@material-ui/icons/ThumbUpAltRounded';
import ChatBubbleRoundedIcon from '@material-ui/icons/ChatBubbleRounded';


const Reviews = () => {
    return(
        <div className ="reviews">
            <div className="reviews__section">
                <div className="reviews__image">
                    <img src="/images/travel.png"></img>
                </div>
                <div className="reviews__container">
                    <div className="reviews__head">
                        <div className="reviews__tag">
                            <h3>SomeOne</h3>
                        </div>
                        <div className="reviews__tag">
                            <h3></h3>
                        </div>
                    </div>
                    <div className="reviews__text">
                        <h3>Our first trip after unlockdown was to
                            Taj Lake Palace Udaipur. It was on my 
                            bucket list and lived up all the 
                            expectations.Right from booking and 
                            support from front desk by Valmiki and 
                            Navneet to the check in at jetty to 
                            royal umbrella welcome to beautiful 
                            room with gorgeous views it was a 
                            complete recovery from all the stress of
                            2020.All protocols were in place and
                            being followed .  
                        </h3>
                    </div>
                    <div className="reviews__bottom">
                        <div className="reviews__bottomItem">
                            <ThumbUpAltRoundedIcon />
                            <h3>Like</h3>
                        </div>
                        <div className="reviews__bottomItem">
                            <ChatBubbleRoundedIcon />
                            <h3>Comment</h3>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
        </div>
    );
}

export default Reviews;