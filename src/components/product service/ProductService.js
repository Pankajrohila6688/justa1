import React, { useEffect, useState } from 'react';
import "../product service/ProductService.css";
import Headers from "../Header";
import Footer from "../Footer";
import PhoneIcon from '@material-ui/icons/Phone';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import MailOutlineRoundedIcon from '@material-ui/icons/MailOutlineRounded';
import LanguageRoundedIcon from '@material-ui/icons/LanguageRounded';
import PersonRoundedIcon from '@material-ui/icons/PersonRounded';
import Reviews from './Reviews';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import { useParams } from 'react-router-dom';
import { db } from '../firebase';
import firebase from "firebase";
import { useStateValue } from '../StateProvider';
import emailjs from 'emailjs-com';

const ProductService = () => {

    const { productServiceId } = useParams();
    const [{ basket, user }, dispatch] = useStateValue();
    const [productDetails, setProductDetails] = useState();


    const [username, setUsername] = useState("");
    const [userPhoneNo, setUserPhoneNo] = useState("");
    const [userEmail, setUserEmail] = useState("");

    useEffect(() => {

        if (productServiceId) {
            console.log(productServiceId)
            db.collection('userform').doc(productServiceId)
            .onSnapshot(snapshot => 
                setProductDetails(snapshot.data())
                // console.log(snapshot.data())
            )
        }

        // db.collection('userform').doc(productServiceId)
        // .collection('details')
        // .orderBy('timestamp', 'asc')
        // .onSnapshot(snapshot =>
        //     setProductDetails(
        //         snapshot.docs.map(doc => doc.data())
        //     )
        // )
    }, [productServiceId])

    // let leads = 0;

    const sendEmail = async (e) => {

        e.preventDefault();    //This is important, i'm not sure why, but the email won't send without it

        emailjs.sendForm('service_2mvsn7k', 'template_xavu779', e.target, 'user_ZKRQ2M4aWbcCtN8SLQxJd')
          .then((result) => {
            console.log(result.text); //  window.location.reload()  //This is if you still want the page to reload (since e.preventDefault() cancelled that behavior) 
          }, (error) => {
              console.log(error.text);
          });
        console.log('Email sent')

        await onFormSubmit(e);
    }

    const onFormSubmit = async (e) => {
        e.preventDefault();

        const docRef = db.collection("productForm").doc();
        docRef.set({
            username: username,
            userPhoneNo: userPhoneNo,
            userEmail: userEmail,
            id: docRef.id,
            productServiceId: productServiceId,
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        });
        setUsername("")
        setUserPhoneNo("")
        setUserEmail("")

        if (productServiceId) {
            const increment = firebase.firestore.FieldValue.increment(1);
            const snapshot =  await db.collection("userform").where("id", "==", productServiceId).where("uid", "!=", user?.uid).get();
            const promises = [];
            snapshot.forEach((doc) => {
                promises.push(doc.ref.update({
                    'leads': increment
                }))
            })
            await Promise.all(promises);
        }

        console.log("form submitted")
        alert("your form has been submitted..")
    }

    const gmail = productDetails?.email;


    return (
        <div className="productService">
            <Headers />
            <div className="productService__Banner">
                <div className="productService__col1">
                    <div className="productService__row1">
                        <img src={productDetails?.imageUrl[0]}></img>
                    </div>
                    <div className="productService__row2">
                        <div className="productService__col2">
                            <img src={productDetails?.imageUrl[1]}></img>
                            <img src={productDetails?.imageUrl[2]}></img>
                        </div>
                        <div className="productService__col2">
                            <img src={productDetails?.imageUrl[3]}></img>
                            <img src={productDetails?.imageUrl[4]}></img>
                        </div>
                    </div>
                </div>
                <div className="productService__TitleBox">                 
                    <div className="productService__Title">
                        <h2>{productDetails?.businessName}</h2>
                        {/* <h3>Near Vk Jain Hospital, Bajaj Road
                        <br /> {productDetails?.businessType}</h3> */}
                        <h3>{productDetails?.address}</h3>
                        <h3>{productDetails?.city}</h3>
                    </div>
                    <div className="productService__TitleButton">
                        <button className="productService__GetInTouch">Get in touch</button>
                    </div>
                </div>
            </div>
            
            
            <div className="productService__Info">
                <div className="productService__InfoBox1">
                    <h2>Description</h2>
                    <h3>
                        {productDetails?.businessDescription}
                    </h3>   
                </div>
                <div className="productService__Infobox2">
                    <div className="productService__InfoHead">
                        <h2>Contact and Address</h2>
                    </div>
                    
                    <div className="productService__InfoContact">
                        <PhoneIcon />
                        <h3>{productDetails?.mobileNo}</h3>
                    </div>
                    <div className="productService__InfoContact">
                        <HomeRoundedIcon />
                        <h3>{productDetails?.address}
                        <br /> {productDetails?.city}</h3>
                    </div>
                    <div className="productService__InfoContact">
                        <MailOutlineRoundedIcon />
                        <h3>{productDetails?.email}</h3>
                    </div>
                    <div className="productService__InfoContact">
                        <LanguageRoundedIcon />
                        <h3>{productDetails?.website}</h3>
                    </div>
                </div>
            </div>
            <div className="productService__Section">
                <div className="productService__FAQuestions">
                    <h2>Frequently Asked Qustions</h2>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[0]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>
                        {productDetails?.answers[0]?.value}
                        </h3>
                    </div>
                    <div className="productService__Question">
                        {/* <h3>How much does it cost to stay at Hotel Taj ?</h3> */}
                        <h3>{productDetails?.questions[1]?.value}</h3>
                    
                    </div>
                    <div className="productService__Answer">
                        {/* <h3>
                            Prices at Hotel Taj Resorts are subject to change
                            according to dates, hotel policy, and other factors.
                            To view prices, please search for the dates you wish
                            to stay at the hotel.
                        </h3> */}
                        <h3>{productDetails?.answers[1]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[2]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[2]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[3]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[3]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[4]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[4]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[5]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[5]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[6]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[6]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[7]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[7]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[8]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[8]?.value}</h3>
                    </div>
                    <div className="productService__Question">
                        <h3>{productDetails?.questions[9]?.value}</h3>
                    </div>
                    <div className="productService__Answer">
                        <h3>{productDetails?.answers[9]?.value}</h3>
                    </div>
                </div>
                <div className="productService__ContactForm">
                        <div className="title">
                            <h2>Get in touch</h2>
                            
                        </div>

                        <form className="form" onSubmit={sendEmail}>
                            <div className="form__items">
                                <PersonRoundedIcon />
                                <input 
                                    required
                                    placeholder="UserName" 
                                    className="input" 
                                    type="text" 
                                    name="username"
                                    value={username} 
                                    onChange={(e) => setUsername(e.target.value)} 
                                />   
                            </div>

                            <div className="form__items">
                                <PhoneIcon />
                                <input 
                                    required
                                    placeholder="Mobile.no" 
                                    className="input" 
                                    type="number" 
                                    name="userPhoneNo"
                                    value={userPhoneNo} 
                                    onChange={(e) => setUserPhoneNo(e.target.value)} 
                                />
                            </div>

                            <div className="form__items">
                                <MailOutlineRoundedIcon />
                                <input
                                    required 
                                    placeholder="Email" 
                                    className="input" 
                                    type="text" 
                                    name="userEmail"
                                    value={userEmail} 
                                    onChange={(e) => setUserEmail(e.target.value)} 
                                />
                            </div>

                            <div className="form__items">
                                {/* <MailOutlineRoundedIcon /> */}
                                <input
                                    style={{ display: "none" }}
                                    required 
                                    placeholder="Email" 
                                    className="input" 
                                    type="text" 
                                    name="gmail"
                                    value={gmail} 
                                    // onChange={(e) => setUserEmail(e.target.value)} 
                                />
                            </div>

                            <div className="form__button">
                            <button type="submit" className="form__buttonDesign">Submit Form</button>
                            </div>
                            
                        </form>
                        
                </div>
            </div>
            <div className="productService__WriteReviews">
                <h2>Write a review</h2>
                <textarea className="productService__ReviewBox" multiline="true"></textarea>
                <button>Submit</button>
            </div>
            <div className="productService__Reviews">
               
                    <h2>Reviews</h2>
                    <Reviews />    
                    <Reviews />    
                    <Reviews />    
                    <Reviews />  
 
            </div>
            <Footer />
        </div>
    );
}

export default ProductService;
