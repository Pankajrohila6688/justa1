import React from "react";
import { Link } from "react-router-dom";
import "./SubCatagories.css"
// import { Divider } from "@material-ui/core";


const SubCatagories = () => {
    return(
        <div className="subcatagories">
            
            <div className={'subcatagories__list'}>
                
                <div className="subcatagories__box">
                    <Link to='/Wedding' style={{ textDecoration: 'none', color: "black" }}>
                        <img src="https://i.imgur.com/kCTOCBQ.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Wedding</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/BabyCare' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/U0S4EbJ.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Baby Care</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                
                <div className="subcatagories__box">
                    <Link to='/Hotels' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/KWXoCXP.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Hotels</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Cinema' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/WCYgyCB.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Cinema Halls</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Tatto' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/Onciitk.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Tatto</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Beauty' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/lM79N8X.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Beauty</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Security' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/DHAm2UG.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Security</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/PetCare' style={{ textDecoration: 'none', color: "black" }}>
                        <img src="https://i.imgur.com/YBtGczr.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Pet Care</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Packers' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/VCExKrB.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Packers And Movers</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/automotive' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/MPRiymi.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Automotive</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/education' style={{ textDecoration: 'none', color: "black" }}>
                        <img src="https://i.imgur.com/o1uCAB0.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Education</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/medical' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/qbAiMVY.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Medical</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/personalcare' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/2HM0BV2.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Personal Care</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/repairs' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/bpnvB8Z.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Repairs</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/restaurant' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/KOARgR4.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Restaurant</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/shopping' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/6OIps4m.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Shopping</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/travel' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/udnoQGC.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Travel</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/groceries' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/kDuClmt.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Groceries</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Internet' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/xF0kScD.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Internet Services</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/HouseKeeping' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/db1siKj.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">House Keeping</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Insuarance' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/II0UzcA.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Insuarance</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Decor' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/cGeYWEW.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Home Decor</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Fitness' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/kbp1rIM.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Fitness</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/BillsAndRecharge' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/t2ohsGa.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Bills and Recharges</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Music' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/kJBMs4N.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Music And Dance</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Advertise' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/UPlqt4j.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Advertising Companies</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
                <div className="subcatagories__box">
                    <Link to='/Sports' style={{ textDecoration: 'none', color: "black"  }}>
                        <img src="https://i.imgur.com/Qpxf5yH.png" className="subcatagories__image" alt=""></img>
                        <h3 className="subcatagories__Text">Sports</h3>
                    </Link>
                </div>
                <hr className="subcatagories__divider" />
            </div>
        </div>
    );
}
export default SubCatagories;