import React from 'react';
import './SidebarOption.css';

const SidebarOption = ({ text, active }) => {
    return (
        <div className={`sidebarOption ${active && 'sidebarOption--active'}`}>
            <h2>{text}</h2>
        </div>
    );
};

export default SidebarOption;