import { Button } from '@material-ui/core';
import React from 'react';
import './Filter.css';

const Filter = () => {
    return (
        <div className="filter">
            <div className="filterButton">
                <Button className="filterButton__button" variant="outlined" color="primary">Popularity</Button>
                <Button className="filterButton__button" variant="outlined" color="primary">Location</Button>
                <Button className="filterButton__button" variant="outlined" color="primary">Availability</Button>
                <Button className="filterButton__button" variant="outlined" color="primary">More filters</Button>
            </div>
        </div>
    );
}

export default Filter;