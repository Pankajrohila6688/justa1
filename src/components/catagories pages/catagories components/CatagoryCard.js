import { Button } from '@material-ui/core';
import React from 'react';
import './CatagoryCard.css';
import { useHistory } from 'react-router-dom';
import { db } from '../../firebase';
import firebase from 'firebase';
import { useStateValue } from '../../StateProvider';

const CatagoryCard = ({ src, name, type, email, city, id }) => {

    const history = useHistory();
    const [{ basket, user }, dispatch] = useStateValue();

    
    const selectCard = async () => {
        if (id) {
            const increment = firebase.firestore.FieldValue.increment(1);
            history.push(`/productService/${id}`)
            // const val = db.collection("userform").doc(id);
            // val.update({ 'views': increment })
            const snapshot =  await db.collection("userform").where("id", "==", id).where("uid", "!=", user?.uid).get();
            const promises = [];
            snapshot.forEach((doc) => {
                promises.push(doc.ref.update({
                    'views': increment
                }))
            })
            await Promise.all(promises);

        }
        else {
            history.push("/")
        }
    }

    return (
        <div className="catagoryCard" onClick={selectCard}>
            <div className="img"><img src={src} alt=""></img></div>
            
            <div className="catagoryCard__info">
                <h2>{name}</h2>
                <h3>{type}</h3>
                <h3>{email}, {city}</h3>
            </div>
            <div className="btn"><Button>Contact</Button></div>
        </div>
    );
}

export default CatagoryCard;