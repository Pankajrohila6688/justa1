import React from "react";
import "./Search.css"
import SearchIcon from "@material-ui/icons/Search";
import { Button } from "@material-ui/core";
const Search = () => {
    return (
        <div className="search">
            <div className="searchButton">
                <Button className="searchButton__button" variant="outlined" color="primary">Select Location</Button>
            </div>
            <div className="searchInput">
                <input type="text" placeholder="search anything" />
                <SearchIcon className="searchIcon"/>
            </div>
        </div>
    );
}
export default Search;