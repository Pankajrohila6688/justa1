import React, { useState, useEffect} from 'react';
import "../catagories style/Travel.css"
// import CatagorySection from './catagories components/CatagorySection';
import SubCatagories from './catagories components/SubCatagories';
import Footer from "../Footer";
import CatagoryCard from "../catagories pages/catagories components/CatagoryCard"
import { db } from '../firebase';
// import Search from './catagories components/Search';
import Filter from "../catagories pages/catagories components/Filter";
// import Search from './catagories components/Search';
// import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';
// import { Button } from '@material-ui/core';


const BabyCare = () => {
   
    const [details, setDetails] = useState([]);
    // const [last, setLast] = useState(null);
    // const [notify, setNotify] = useState("");
    // const [list, setList] = useState([]);
    // const [nextPosts_loading, setNextPostsLoading] = useState(false);

    useEffect(() => {

        db.collection('userform').where("businessType", "==", "Baby Care").onSnapshot(snapshot => 
            setDetails(snapshot.docs.map(doc => ({
                id: doc.id,
                name: doc.data()
            })))
        )
    },[]);

    

    return (
        <div className="catagories"> 
        <div className="catagory__Container">
            <div className="catagory__SubCatagories">
                <SubCatagories />
            </div> 
            <h2 className="catagoryBanner__title">Baby Care</h2>
            <div className="catagory__Filter">
                <Filter />
            </div>
            <div className="catagory__ServicesCard">
                {
                    details && details.map(detail => {
                        return (
                            <CatagoryCard 
                                name={detail.name.businessName}
                                type={detail.name.businessType}
                                city={detail.name.city}
                                email={detail.name.email}
                                src={detail.name.singleImageUrl}
                                id={detail.id}
                            />
                        )
                    })
                }
            </div>
        
        </div> 
            

        <div className="catagory__footer">
            <Footer />
        </div>
    </div>
    );
};
export default BabyCare;