import React, { useState, useEffect} from 'react';
import "../catagories style/Travel.css"
// import CatagorySection from './catagories components/CatagorySection';
import SubCatagories from './catagories components/SubCatagories';
import Footer from "../Footer";
import CatagoryCard from "../catagories pages/catagories components/CatagoryCard"
import { db } from '../firebase';
// import Search from './catagories components/Search';
import Filter from "../catagories pages/catagories components/Filter";
// import Search from './catagories components/Search';
// import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';
// import { Button } from '@material-ui/core';


const Travel = () => {
   
    const [details, setDetails] = useState([]);
    // const [last, setLast] = useState(null);
    // const [notify, setNotify] = useState("");
    // const [list, setList] = useState([]);
    // const [nextPosts_loading, setNextPostsLoading] = useState(false);
    
    // // useEffect(() => {
    // //     db.collection('userform').where("businessType", "==", "Travel").onSnapshot(snapshot => (
    // //         setDetails(snapshot.docs.map(doc => doc.data()))
    // //     ))
    // // }, [])

    useEffect(() => {

        db.collection('userform').where("businessType", "==", "Travel").onSnapshot(snapshot => 
            setDetails(snapshot.docs.map(doc => ({
                id: doc.id,
                name: doc.data()
            })))
        )
        
        // setNextPostsLoading(true);

        // db.collection('userform')
        // .where("businessType", "==", "Travel")
        //  .orderBy('timestamp', "desc")
        //  .limit(3)
        //  .get()
        //  .then((querySnapshot) => {
        //     const lastVisible = querySnapshot.docs[querySnapshot.docs.length - 1];
        //     setLast(lastVisible.data());

        //     console.log(lastVisible.data());
     
        //     const postList = []
        //     querySnapshot.forEach((doc) => {
        //       postList.push(doc.data()); 
        //     })
        //     setDetails(postList);
        //  })
        //  .catch((err) => {
        //     console.log(err)
        //  })
    },[]);

    // const fetchMoreData = () => {
    //     const field = "timestamp";

    //     setNextPostsLoading(true);

    //     db.collection('userform')
    //         .where("businessType", "==", "Travel")
    //         .orderBy(field, "desc")
    //         .startAfter(last[field])
    //         .limit(3)
    //         .get()
    //         .then((querySnapshot) => {
    //             const lastVisible = querySnapshot.docs[querySnapshot.docs.length - 1];

    //             if (lastVisible === undefined) {
    //                 setNotify("nothing to load.");
    //                 setNextPostsLoading(false);
    //                 alert("you have reached upto last")
    //                 console.log("nothing to load")
    //                 return;
    //             }

    //             const postList = [];

    //             querySnapshot.forEach((doc) => {
    //               postList.push(doc.data());
    //             })
    //             console.log(postList)
         
    //             if (lastVisible !== undefined) {
    //               setDetails([...details, ...postList]);
    //               setLast(lastVisible.data());
    //               console.log(lastVisible.data())
    //               setNextPostsLoading(false)
    //             } 
    //             // else {
    //             //   setNotify("nothing to load.");
    //             //   setNextPostsLoading(false);
    //             //   console.log("nothing to load")
    //             //   return;
    //             // }
                
    //         })
    //         .catch((err) => {
    //            console.log(err)
    //            setNextPostsLoading(false);
    //         })
    // };

    return (
        <div className="catagories"> 
        <div className="catagory__Container">
            <div className="catagory__SubCatagories">
                <SubCatagories />
            </div> 
            <h2 className="catagoryBanner__title">Travel</h2>
            <div className="catagory__Filter">
                <Filter />
            </div>
            <div className="catagory__ServicesCard">
                {
                    details && details.map(detail => {
                        return (
                            <CatagoryCard 
                                name={detail.name.businessName}
                                type={detail.name.businessType}
                                city={detail.name.city}
                                email={detail.name.email}
                                src={detail.name.singleImageUrl}
                                id={detail.id}
                            />
                        )
                    })
                }

                {/* {nextPosts_loading ? (
                    <p className="loading" >Loading..</p>
                ) : last !== undefined ? (
                    <Button variant="outlined" className="loadMore__btn" onClick={() => fetchMoreData()}>Show More</Button>
                    // <ArrowDropDownCircleIcon className="loadMore__btn" onClick={() => fetchMoreData()} />
                ) : (
                    <span>You are up to date!</span>
                )} */}
            </div>
        
        </div> 
            

        <div className="catagory__footer">
            <Footer />
        </div>
    </div>
    );
};
export default Travel;







// const [posts, setPosts] = useState([]);
    // const [lastKey, setLastKey] = useState("");
    // const [nextPosts_loading, setNextPostsLoading] = useState(false);

    // useEffect(() => {
    //     // first 5 posts
    //     Post.postsFirstBatch()
    //     .then((res) => {
    //         setPosts(res.posts);
    //         setLastKey(res.lastKey);
    //     })
    //     .catch((err) => {
    //         console.log(err);
    //     });
    // }, []);

    // /**
    //  * used to apply pagination on posts
    //  * @param {String} key
    //  * @return next batch of posts (+5 posts)
    //  * will be fired when user click on 'More Posts' button.
    //  */
    // const fetchMorePosts = (key) => {
    //     if (key.length > 0) {
    //     setNextPostsLoading(true);
    //     Post.postsNextBatch(key)
    //         .then((res) => {
    //         setLastKey(res.lastKey);
    //         setPosts(posts.concat(res.posts));
    //         setNextPostsLoading(false);
    //         })
    //         .catch((err) => {
    //         console.log(err);
    //         setNextPostsLoading(false);
    //         });
    //     }
    // };



