import React from "react";
import "./Showcase.css"
const Showcase = () => {
    return(
        <div className="aboutUs">
            <div className="aboutUs__header">
                <h2>About Us</h2>
            </div>
            <div className="aboutUs__content">   
                <img className="about__image" src="/images/aboutUs.png"></img>
                <div className="aboutUs__discription">
                    <div className="aboutUs__card">
                        <h2>Best Indian Business Directory</h2>
                        <h3>Justa1 is one of the best and fastest growing 
                            Indian business directory, which provides quality 
                            business leads to the business owners and 
                            help to grow their revenue.
                        </h3>
                    </div>
                    <div className="aboutUs__card">
                        <h2>Best Indian Business Directory</h2>
                        <h3>Justa1 is one of the best and fastest growing 
                            Indian business directory, which provides quality 
                            business leads to the business owners and 
                            help to grow their revenue.
                        </h3>
                    </div>
                    <div className="aboutUs__card">
                        <h2>Best Indian Business Directory</h2>
                        <h3>Justa1 is one of the best and fastest growing 
                            Indian business directory, which provides quality 
                            business leads to the business owners and 
                            help to grow their revenue.
                        </h3>
                    </div>                    
                </div>
            </div>
        </div>
    );
}
export default Showcase;