import React from 'react';
import './Sponsers.css';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';  
const Sponsers = () => {
    const items = [
        <img src="https://i.imgur.com/ufEvk0Z.jpg" className="sliderImage" alt="" />,
        <img src="https://i.imgur.com/rTx7mET.jpg"className="sliderImage" alt="" />,
        <img src="https://i.imgur.com/NDjINrq.jpg" className="sliderImage" alt="" />,
        <img src="https://i.imgur.com/J1GZpNR.jpg"className="sliderImage" alt="" />,
        <img src="https://i.imgur.com/95xS731.jpg"className="sliderImage" alt="" />,
    ]
    // const discription = [
    //     <h2 className="sliderDiscription"></h2>,
    //     <h2 className="sliderDiscription"></h2>,
    //     <h2 className="sliderDiscription"></h2>,
    //     <h2 className="sliderDiscription"></h2>,
    // ]
    const settings = {
        autoPlay: true,
        autoPlayInterval: 2000,
        mouseTracking: true,
        swipeable: true,
        animationDuration: 700,
        autoPlayStrategy: 'none',
        infinite: true,
        autoHeight: true,
        disableButtonsControls: true,
        disableDotsControls: true,
    }
    return (
        <div className="sponsers">
            <div className="sponsers__section">
                <AliceCarousel {...settings} className="alice-carousel" items={items} />
            </div>
        </div>
    )
    }
export default Sponsers;