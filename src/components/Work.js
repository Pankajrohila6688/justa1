import React from 'react';
import './Work.css'
// import Slider from 'react-slick';
import "./sections/Slide"
const work = () => {
    return (
        <div id="work" className="work">
            <div className="work__header">
                <h2>Why Just A1 ?</h2>
                <hr/>
                <div className="work__info">
                    <img src="https://i.imgur.com/1YrPtdQ.png" alt=""></img>
                    <div className="work__infoContainer">
                        <div className="work__infoBox">
                            <h2>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                            sed do eiusmod tempor incididunt
                            </h2>
                        </div>
                        <div className="work__infoBox">
                            <h2>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                            sed do eiusmod tempor incididunt
                            </h2>
                        </div>
                        <div className="work__infoBox">
                            <h2>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                            sed do eiusmod tempor incididunt
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};
export default work;