import React from 'react';
import './Plans.css';
const Plans = () => {
    return (
        <div class="pricing-table">
            <h1 class="pricing-header">Our Plans</h1>
            <hr />
            <div class="pricing-container">
                <div class="pricing-card">
                    <h3 class="pricing-card-header">Personal</h3>
                    <div class="price"><sup>₹</sup>0<span>/MO</span></div>
                        <ul>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                
                        </ul>
                    <button class="order-btn">Order Now</button>
                </div>

                <div class="pricing-card">
                    <h3 class="pricing-card-header">Professional</h3>
                    <div class="price"><sup>₹</sup>0<span>/MO</span></div>
                        <ul>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                        </ul>
                    <button class="order-btn">Order Now</button>
                </div>

                <div class="pricing-card">
                    <h3 class="pricing-card-header">Premium</h3>
                    <div class="price"><sup>₹</sup>0<span>/MO</span></div>
                        <ul>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                            <li>List Business</li>
                        </ul>
                    <button class="order-btn">Order Now</button>
                </div>
            </div>
        </div>

    );
}
export default Plans;