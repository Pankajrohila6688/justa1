import React, { useEffect, useState } from 'react';
import { db } from '../firebase';
import { useStateValue } from '../StateProvider';
import './Details.css';
// import Detail from './Detail';
import DetailCard from "./DetailCard";
import Header from "../Header";
import Scroll from 'react-scroll';

import Footer from "../Footer";
// import CatagoryCard from '../catagories pages/catagories components/CatagoryCard';


const Details = () => {

    const ScrollLink = Scroll.ScrollLink;
    const [{ basket, user }, dispatch] = useStateValue();
    const [details, setDetails] = useState([]);
    // const [name, setName] = useState("");
    const [loading, setLoading] = useState(true);

    const delay = ms => new Promise(res => setTimeout(res, ms));
    const yourFunction = async () => {
        await delay(6000);
        setLoading(false)
    };

    useEffect(() => {
        yourFunction();
        if(user) {
            // console.log(user.email)
            // console.log(user.uid)
            db
            .collection('userform')
            .where("uid", "==", user?.uid)
            // .orderBy('timestamp', 'desc')
            .onSnapshot(snapshot => (
                setDetails(snapshot.docs.map(doc => ({
                    id: doc.id,
                    name: doc.data()
                })))
                // setDetails(snapshot.docs.map(doc => doc.data()))
            ))
        } else {
            yourFunction();
            setDetails([]); 
        }

    }, [user])

    return (
        <div className="details">
            <Header />
            <h1>Business Details</h1>
            <hr />

            {loading ? ( 
                <p className="detailsLoading">Loading...</p>
            ): (
                <>
                    {details?.map(detail => (
                        <DetailCard 
                            businessName={detail.name.businessName}
                            type={detail.name.businessType}
                            tagline={detail.name.tagline}
                            description={detail.name.businessDescription}
                            email={detail.name.email}
                            mobileNo={detail.name.mobileNo}
                            website={detail.name.website}
                            address={detail.name.address}
                            city={detail.name.city}
                            src={detail.name.imageUrl}
                            id={detail.id}
                            views={detail.name.views}
                            leads={detail.name.leads}
                        />
                    ))}
                </>
            )}
                
            <Footer />
        </div>
    );
}

export default Details;