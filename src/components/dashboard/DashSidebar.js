import React from 'react'
import DashboardIcon from '@material-ui/icons/Dashboard';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import CreateIcon from '@material-ui/icons/Create';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import  './DashSidebar.css';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import HeadsetMicIcon from '@material-ui/icons/HeadsetMic';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';
import { useStateValue } from '../StateProvider';
import { useHistory } from 'react-router-dom';

const DashSidebar = () =>{

    const [{ basket, user }, dispatch] = useStateValue();
    const history = useHistory();

    return (
        <div className="dashSide">
        
            <div className="dashSide_Header">
                    <h2>Just A1</h2>
                    <h3>
                       Welcome {user?.email}
                    </h3>
            </div>
            <hr className="dashSide__divider" />
            <div className="dashSide__Options" onClick={() => history.push('/dash')} >
                <DashboardIcon />
                <h3>Dashboard</h3>
            </div>
            <hr className="dashSide__divider" />
            <div className="dashSide__Options" onClick={() => history.push('/dashBusiness')} >
                <BusinessCenterIcon /> 
                <h3>Business Details</h3>
            </div>
            <hr className="dashSide__divider" />
            <div className="dashSide__Options">
                <CreateIcon />
                <h3>Reviews</h3>
            </div>
            <hr className="dashSide__divider" />
            <div className="dashSide__Options">
                <RecordVoiceOverIcon />
                <h3>Announcements</h3>
            </div>
            <hr className="dashSide__divider" />
            <div className="dashSide__Options">
                <AddToQueueIcon />
                <h3>Advertise</h3>
            </div>
            <hr className="dashSide__divider" />
            <div className="dashSide__Options">
                <AccountBalanceWalletIcon  />
                <h3>Payment & Payout</h3>
            </div>
            <hr className="dashSide__divider" />
            <div className="dashSide__Options">
                <HeadsetMicIcon />
                <h3>Help Service</h3>
            </div>
        </div>
    );
}
export default DashSidebar;