import React, { useEffect, useState } from 'react';
import { db } from '../firebase';
import Header from '../Header'
import DashSidebar from "./DashSidebar"
import "./Dash.css";
import UserDash from "./UserDash"
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import DashboardIcon from '@material-ui/icons/Dashboard';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import CreateIcon from '@material-ui/icons/Create';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import { useStateValue } from '../StateProvider';


const Dash = () => {

    const [details, setDetails] = useState([]);
    const [{ basket, user }, dispatch] = useStateValue();


    useEffect(() => {
        // yourFunction();
        if(user) {
            // console.log(user.email)
            // console.log(user.uid)
            db
            .collection('userform')
            .where("uid", "==", user?.uid)
            // .orderBy('timestamp', 'desc')
            .onSnapshot(snapshot => (
                setDetails(snapshot.docs.map(doc => ({
                    id: doc.id,
                    name: doc.data()
                })))
                // setDetails(snapshot.docs.map(doc => doc.data()))
            ))
        } else {
            // yourFunction();
            setDetails([]); 
        }

    }, [user])

    return (
        <div className="dash">
            <div className="dash__left">
                <DashSidebar />
            </div>
            <div className="dash__right">
                {/* <UserDash /> */}
                {details?.map(detail => (
                        <UserDash 
                            // businessName={detail.name.businessName}
                            // type={detail.name.businessType}
                            // tagline={detail.name.tagline}
                            // description={detail.name.businessDescription}
                            // email={detail.name.email}
                            // mobileNo={detail.name.mobileNo}
                            // website={detail.name.website}
                            // address={detail.name.address}
                            // city={detail.name.city}
                            // src={detail.name.imageUrl}
                            // id={detail.id}
                            views={detail.name.views}
                            leads={detail.name.leads}
                            id={detail.id}
                        />
                    ))}
                
            </div>
        </div>
    )
}

export default Dash;