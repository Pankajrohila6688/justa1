import React from 'react';
import './Dashboard.css';
import Footer from "../Footer";
import { Link } from 'react-router-dom';
const Dashboard = () => {

    return (
        <div className="dashboard">
            <h2>Dashboard</h2>
            <hr />
        <div className="dashboard__content">
            <Link to="/details" style={{ textDecoration: 'none', color: "black"  }} >
            <div className="dashboard__card">
                <img src="/images/business.png"/>
                <h2>Business Info</h2>
                <h3>Modify all your listed business</h3>
            </div>
            </Link>
            <div className="dashboard__card">
                <img src="/images/payment.png" />
                <h2>Payment Details</h2>
                <h3>Rewiew Payment Plans</h3>
            </div>
            <div className="dashboard__card">
                <img src="/images/privacy.png"/>
                <h2>Privacy & Policy</h2>
                <h3>Just A1 pirvacy terms for users business</h3>
            </div>
            <div className="dashboard__card">
                <img src="/images/service.png"/>
                <h2>JustA1 Support </h2>
                <h3>Always ready to help customers on every step</h3>
            </div>
        </div>
        <Footer />
        </div>
        
    )
}

export default Dashboard;
