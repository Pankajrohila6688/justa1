import React, { useEffect, useState } from 'react'
import './DetailCard.css';
import { useHistory } from 'react-router-dom';
import { db } from '../firebase';
import Scroll from 'react-scroll'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CatagoryCard from '../catagories pages/catagories components/CatagoryCard';

import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import { storage } from '../firebase';

import {Anchor} from "antd";
const { Link } = Anchor;
const  DetailCard = ({ src, city, address, website, businessName, tagline, description, type, area, email,mobileNo,landmark,handleEditClick, handleDeleteClick, id, views, leads }) => {
    const ScrollLink = Scroll.ScrollLink

    // const [image, setImage] = useState(null);
    // const [url1, setUrl1] = useState([]);
    // const [progress, setProgress] = useState(0);

    const [open, setOpen] = useState(false);

    const history = useHistory();
    const [name, setName] = useState("");
    const [newTagline, setNewTagline] = useState("");
    const [newCity, setNewCity] = useState("");
    const [newWebsite, setNewWebsite] = useState("");
    const [newMobileNo, setNewMobileNo] = useState("");
    const [newAddress, setNewAddress] = useState("");
    const [newEmail, setNewEmail] = useState("");
    const [newDescription, setNewDescription] = useState("");
    // const [disabled, setDisabled] = useState(false);

    const [details, setDetails] = useState([]);

    useEffect(() => {
        db.collection('productForm').where("productServiceId", "==", id).onSnapshot(snapshot => 
            setDetails(snapshot.docs.map(doc => ({
                id: doc.id,
                name: doc.data()
            }))) 
        )
    }, [])


    const selectCard1 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                businessName: name,
            })
            setName("");
        }
    }

    const selectCard2 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                tagline: newTagline,
            })
            setNewTagline("");
        }
    }

    const selectCard3 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                address: newAddress,
            })
            setNewAddress("");
        }
    }

    const selectCard4 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                city: newCity,
            })
            setNewCity("");
        }
    }

    const selectCard5 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                website: newWebsite,
            })
            setNewWebsite("");
        }
    }

    const selectCard6 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                mobileNo: newMobileNo,
            })
            setNewMobileNo("");
        }
    }

    const selectCard7 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                email: newEmail,
            })
            setNewEmail("");
        }
    }

    const selectCard8 = () => {
        if (id) {
            console.log(id)
            db.collection("userform").doc(id).update({
                businessDescription: newDescription,
            })
            setNewDescription("");
        }
    }

    // const handleChangeImage1 = (e) => {
    //     if (e.target.files[0]) {
    //         setImage(e.target.files[0]);
    //     }
    // }

    // const singleImage = () => {
    //     const promise = [];

    //     const uploadTask = storage.ref(`images/${image.name}`).put(image);
    //     uploadTask.on(
    //         "state_changed",
    //         (snapshot) => {
    //           // progress function ...
    //           const progress = Math.round(
    //             (snapshot.bytesTransferred / snapshot.totalBytes) * 100
    //           );
    //           setProgress(progress);
    //         },
    //         (error) => {
    //           // Error function ...
    //           console.log(error);
    //         },
    //         () => {
    //           // complete function ...
    //           storage
    //             .ref("images")
    //             .child(image.name)
    //             .getDownloadURL()
    //             .then((url) => {
    //               setUrl1(url);
    //               url1.push(url);
    //             });
    //         }
    //     )
    //     Promise.all(promise);
    // }

    // const imageEdit = async() => {
        
    //     if (id) {
    //         await singleImage();
    //         console.log(id)
    //         db.collection("userform").doc(id).update({
    //             imageUrl: url1,
    //         })
    //         setNewDescription("");
    //     }

    // }

    const handleClickOpen = () => {
        setOpen(true);
      };
    
      const handleClose = () => {
        setOpen(false);
      };

    const deleteDoc = () => {
        if (id) {
            db.collection('userform').doc(id).delete();
        }
        setOpen(false);
    }

    return (
        <div className="detailCard">
        
            <div className="detailCard__container">
                <div className="detailCard__top">
                    <h2>Total Views : {views}</h2>

                    <h2>Total leads : {leads}</h2>

                    {
                        details && details.map(detail => {
                            return (
                                <CatagoryCard 
                                    name={detail.name.username}
                                    type={detail.name.businessType}
                                    email={detail.name.userEmail}
                                    mobileNo={detail.name.userPhoneNo}
                                    src={detail.name.singleImageUrl}
                                    id={detail.id}
                                />
                            )
                        })
                    }

                    <Anchor underline="false">
                        <Link underline="false"className="buttonTop" href="#form" title="Edit Business Details"/>
                    </Anchor>

                    {/* <button onClick={deleteDoc}>Delete</button> */}

                    <div>
                        <Button variant="outlined" color="primary" onClick={handleClickOpen}>Delete your Business</Button>
                        <Dialog
                            open={open}
                            onClose={handleClose}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogTitle id="alert-dialog-title">{"Do you really want to delete your listed Business?"}</DialogTitle>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                    Your Listed business will be deleted permanently, You can not undo it again.
                                    Do you want to delete your listed Business? 
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} variant="contained" color="primary">
                                    Cancel
                                </Button>
                                <Button onClick={deleteDoc} variant="contained" color="secondary" autoFocus>
                                    Delete
                                </Button>
                            </DialogActions>
                        </Dialog>
                    </div>
                    
                </div>
                
                <div className="detailCard__left-right">
                    <div className="detailCard__left">
                        <div className="detailCard__content">
                            <h2>BusinessName:</h2>
                            <h3>{businessName}</h3>
                        </div>
                        <div className="detailCard__content">
                            <h2>Tagline:</h2>
                            <h3>{tagline}</h3>
                        </div>
                        <div className="detailCard__content">
                            <h2>Catagory:</h2>
                            <h3>{type}</h3>
                        </div>
                        <div className="detailCard__content">
                            <h2>E-Mail:</h2>
                            <h3>{email}</h3>
                        </div>
                        <div className="detailCard__content">
                            <h2>Mobile Number:</h2>
                            <h3>{mobileNo}</h3>
                        </div>
                        <div className="detailCard__content">
                            <h2>Website:</h2>
                            <h3>{website}</h3>
                        </div>
                        <div className="detailCard__content">
                            <h2>Address:</h2>
                            <h3>{address}</h3>
                        </div>
                        <div className="detailCard__content">
                            <h2>City:</h2>
                            <h3>{city}</h3>
                        </div>
                    </div>    
                    <div className="detailCard__right">
                        <img src={src[0]} alt=""></img>

                        <img src={src[1]} alt=""></img>
                        <img src={src[2]} alt=""></img>
                        <img src={src[3]} alt=""></img>
                    </div>
                </div>
                <div className="detailCard__bottom">
                    <h2>Description:</h2>
                        <h3>{description}</h3>
                </div>
            </div>
            
                <div className="form__div1">
                    <br />
                    <h1>Edit Your Businenss Details</h1>
                   
                    <hr className="form__hr"/>
                    <br />

                    <h3>Edit Business Name *</h3>
                    <input 
                        // required
                        placeholder={businessName}
                        variant="outlined" 
                        name="businessName"
                        value={name} 
                        onChange={(e) => setName(e.target.value)} 
                    />
                    <button disabled={!name} className="detailCard__edit" onClick={selectCard1}>Save</button>

                    <h3>Edit Tagline</h3>
                    <input 
                        // required
                        placeholder={tagline}
                        variant="outlined" 
                        name="tagline"
                        value={newTagline} 
                        onChange={(e) => setNewTagline(e.target.value)} 
                    /> 
                    <button disabled={!newTagline} className="detailCard__edit" onClick={selectCard2}>Save</button>

                    <h3>Edit Address</h3>
                    <input 
                        // required
                        placeholder={address} 
                        variant="outlined" 
                        name="address"
                        value={newAddress} 
                        onChange={(e) => setNewAddress(e.target.value)} 
                    />    
                    <button disabled={!newAddress} className="detailCard__edit" onClick={selectCard3}>Save</button>

                    <h3>Edit City</h3>
                    <input 
                        // required
                        placeholder={city}
                        variant="outlined" 
                        name="city"
                        value={newCity} 
                        onChange={(e) => setNewCity(e.target.value)} 
                    />
                    <button disabled={!newCity} className="detailCard__edit" onClick={selectCard4}>Save</button>

                    <h3>Edit Website</h3>
                    <input 
                        // required
                        placeholder={website}
                        variant="outlined" 
                        name="website"
                        value={newWebsite} 
                        onChange={(e) => setNewWebsite(e.target.value)} 
                    />
                    <button disabled={!newWebsite} className="detailCard__edit" onClick={selectCard5}>Save</button>

                    <h3>Edit Phone Number</h3>
                    <input 
                        // required
                        placeholder={mobileNo}
                        variant="outlined" 
                        name="phoneNumber"
                        value={newMobileNo} 
                        onChange={(e) => setNewMobileNo(e.target.value)} 
                    />
                    <button disabled={!newMobileNo} className="detailCard__edit" onClick={selectCard6}>Save</button>   
                </div>
            <div  className="form__div8">
                    {/* <br />
                    <h3>Edit Contact Email</h3> */}
                    <hr className="form__hr"/>
                    <br />

                    <h3>Edit Email</h3>
                    <input 
                        // required
                        placeholder={email}
                        variant="outlined" 
                        name="email"
                        value={newEmail} 
                        onChange={(e) => setNewEmail(e.target.value)} 
                    />
                    <button disabled={!newEmail} className="detailCard__edit" onClick={selectCard7}>Save</button>
            </div>
            <div className="form__div6">
                    <br />
                    {/* <h3>Edit More Info</h3>
                    <hr className="form__hr"/>
                    <br /> */}

                    <h3>Edit Business Description</h3>
                    <textarea 
                        // required
                        placeholder={description}
                        label="Business Description" 
                        variant="outlined" 
                        multiline="true"
                        rowsMax="Infinity"
                        name="businessDescription"
                        value={newDescription} 
                        onChange={(e) => setNewDescription(e.target.value)}  
                    />
                    <button disabled={!newDescription} className="detailCard__edit" onClick={selectCard8}>Save</button>
            </div>
            {/* <div className="form__div7">
                    <br />
                    <h3>Media</h3>
                    <hr className="form__hr"/>
                    <br />

                    <label className="form__image">
                        <input className="custom-file-input" type="file" onChange={handleChangeImage1}/>
                        <AddAPhotoIcon className="icon"/> 
                        <p>Add Photo</p>
                    </label>
                    
                    <label className="form__image">
                        <input className="custom-file-input" type="file" onChange={handleChange} multiple/>
                        <AddAPhotoIcon className="icon"/> 
                        <p>Add Photo</p>
                    </label>

                    
                <progress className="imageupload__progress" value={progress} max="100" />
                <button className="detailCard__edit" onClick={imageEdit}>Save</button>
            </div> */}
                
        </div>
    )
}


export default DetailCard
