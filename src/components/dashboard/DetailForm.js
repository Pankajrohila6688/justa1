import React from 'react'

const DetailForm = ({src, businessName, setbusinessName,tagline,description, setdescription, type, area, email,setemail,mobileNo,setmobileNo,landmark,handleEditClick, handleDeleteClick}) => {
    return (
        <div className="detailForm">
            <div className="inputGroup">
                <label htmlFor="businessName">BusinessName</label>
                <input
                    type="text"
                    name="businessName"
                    value={businessName}
                    onChange={(e) => setbusinessName(e.target.value)}
                />
            </div>
            {/* <div className="inputGroup">
                <label htmlFor="tagline">Tagline</label>
                <input
                    type="text"
                    name="tagline"
                    value={tagline}
                    onChange={(e) => settagline(e.target.value)}
                />
            </div> */}
            <div className="inputGroup">
                <label htmlFor="description">Description</label>
                <input
                    type="text"
                    name="description"
                    value={description}
                    onChange={(e) => setdescription(e.target.value)}
                />
            </div>
            <div className="inputGroup">
                <label htmlFor="email">E-mail</label>
                <input
                    type="text"
                    name="email"
                    value={email}
                    onChange={(e) => setemail(e.target.value)}
                />
            </div>
            <div className="inputGroup">
                <label htmlFor="mobileNo">Mobile Number</label>
                <input
                    type="text"
                    name="mobileNo"
                    value={mobileNo}
                    onChange={(e) => setmobileNo(e.target.value)}
                />
            </div>
        </div>
    )
}

export default DetailForm;
