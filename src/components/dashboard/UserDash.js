import React, { useEffect, useState } from 'react';
import { db } from '../firebase';
import "./UserDash.css";
import CatagoryCard from '../catagories pages/catagories components/CatagoryCard';

const UserDash = ({ views, leads, id }) => {

    const [details, setDetails] = useState([]);

    useEffect(() => {
        db.collection('productForm').where("productServiceId", "==", id).onSnapshot(snapshot => 
            setDetails(snapshot.docs.map(doc => ({
                id: doc.id,
                name: doc.data()
            }))) 
        )
    }, [])

    

    return (
        <div classsName="userDash">
            <h2 className="userDash__Text"></h2>
            <div className="userDash__Container">
                <div className="userDash__Card">
                    <h2>User Views</h2>
                    <h1>{views}</h1>
                    <h2>In All Times</h2>
                </div>
                <div className="userDash__Card">
                    <h2>User Leads</h2>
                    <h1>{leads}</h1>
                    <h2>In All Times</h2>
                </div>
                <div className="userDash__Card">
                    <h2>User Reviews</h2>
                    <h1>0</h1>
                    <h2>In All Times</h2>
                </div>
            </div>
            <div className="userDash__Reach">
                <h2>Active Reach</h2>
            </div>
            
            <div className="userDash__Box">
                
                {
                    details && details.map(detail => {
                        return (
                            // <CatagoryCard 
                            //     name={detail.name.username}
                            //     type={detail.name.businessType}
                            //     email={detail.name.userEmail}
                            //     mobileNo={detail.name.userPhoneNo}
                            //     src={detail.name.singleImageUrl}
                            //     id={detail.id}
                            // />
                            <div className="userDash__BoxBottom">
                                <h3>{detail.name.username}</h3>
                                <h3>{detail.name.userPhoneNo}</h3>
                                <h3>{detail.name.userEmail}</h3>
                                <h3>Delete</h3> 
                            </div>
                            
                        )
                    })
                }

                {/* <hr /> */}
                {/* <div className="userDash__BoxBottom">
                    <h3>Shobhit Purva</h3>
                    <h3>9166012652</h3>
                    <h3>purvashobhit@gmail.com</h3>
                    <h3>Delete</h3>
                </div>
                <hr />
                <div className="userDash__BoxBottom">
                    <h3>Shobhit Purva</h3>
                    <h3>9166012652</h3>
                    <h3>purvashobhit@gmail.com</h3>
                    <h3>Delete</h3>
                </div>
                <hr />
                <div className="userDash__BoxBottom">
                    <h3>Shobhit Purva</h3>
                    <h3>9166012652</h3>
                    <h3>purvashobhit@gmail.com</h3>
                    <h3>Delete</h3>
                </div>
                <hr />
                <div className="userDash__BoxBottom">
                    <h3>Shobhit Purva</h3>
                    <h3>9166012652</h3>
                    <h3>purvashobhit@gmail.com</h3>
                    <h3>Delete</h3>
                </div>
                <hr />
                <div className="userDash__BoxBottom">
                    <h3>Shobhit Purva</h3>
                    <h3>9166012652</h3>
                    <h3>purvashobhit@gmail.com</h3>
                    <h3>Delete</h3>
                </div>
                <hr />
                <div className="userDash__BoxBottom">
                    <h3>Shobhit Purva</h3>
                    <h3>9166012652</h3>
                    <h3>purvashobhit@gmail.com</h3>
                    <h3>Delete</h3>
                </div>
                <hr />
                <div className="userDash__BoxBottom">
                    <h3>Shobhit Purva</h3>
                    <h3>9166012652</h3>
                    <h3>purvashobhit@gmail.com</h3>
                    <h3>Delete</h3>
                </div>
                <hr /> */}
            </div>
        </div>
    );
}

export default UserDash;