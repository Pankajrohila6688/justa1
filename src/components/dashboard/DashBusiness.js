import React, { useEffect, useState } from 'react';
import { db } from '../firebase';
import { useStateValue } from '../StateProvider';
import DashSidebar from "./DashSidebar"
import "./Dash.css";
import DashEdit from "./DashEdit";

const DashBusiness = () => {

    const [{ basket, user }, dispatch] = useStateValue();
    const [details, setDetails] = useState([]);
    const [loading, setLoading] = useState(true);

    const delay = ms => new Promise(res => setTimeout(res, ms));
    const yourFunction = async () => {
        await delay(6000);
        setLoading(false)
    };


    useEffect(() => {
        yourFunction();
        if(user) {
            // console.log(user.email)
            // console.log(user.uid)
            db
            .collection('userform')
            .where("uid", "==", user?.uid)
            // .orderBy('timestamp', 'desc')
            .onSnapshot(snapshot => (
                setDetails(snapshot.docs.map(doc => ({
                    id: doc.id,
                    name: doc.data()
                })))
                // setDetails(snapshot.docs.map(doc => doc.data()))
            ))
        } else {
            yourFunction();
            setDetails([]); 
        }

    }, [user])

    return (
        <div className="dash">
            <div className="dash__left">
                <DashSidebar />
            </div>
            <div className="dash__right">

                {loading ? (
                    <p className="dashLoading">Loading...</p>
                ) : (
                    <>
                    {details?.map(detail => (
                        <DashEdit 
                            businessName={detail.name.businessName}
                            type={detail.name.businessType}
                            tagline={detail.name.tagline}
                            description={detail.name.businessDescription}
                            email={detail.name.email}
                            mobileNo={detail.name.mobileNo}
                            website={detail.name.website}
                            address={detail.name.address}
                            city={detail.name.city}
                            src={detail.name.imageUrl}
                            id={detail.id}
                        />
                    ))}
                    </>
                )}
            </div>
        </div>
    )
}

export default DashBusiness
