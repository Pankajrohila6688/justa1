import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBqQKMVQsFlqnYSINXHE2SKm5SZgzV64Tg",
    authDomain: "justa1.firebaseapp.com",
    projectId: "justa1",
    storageBucket: "justa1.appspot.com",
    messagingSenderId: "1040972535445",
    appId: "1:1040972535445:web:19a6923eaf81ae5fb7ad9c",
    measurementId: "G-40LL4XBV2M"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({ experimentalForceLongPolling: true });

const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider, storage };
export { db };
