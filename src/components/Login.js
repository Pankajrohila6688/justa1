import React, {useState} from 'react';
import './Login.css';
import { Button, TextField } from '@material-ui/core';
import { auth, provider } from './firebase';
import { Link, useHistory } from 'react-router-dom';
import { useStateValue } from './StateProvider';
import { actionTypes } from './reducer';
import { useForm } from "react-hook-form";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from '@material-ui/icons/Close';
// import emailjs from 'emailjs-com';

const Login = () => {
    const history = useHistory()
    const [{}, dispatch] = useStateValue();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // const sendEmail = (e) => {
    //     e.preventDefault();
    //     emailjs.sendForm('service_2mvsn7k', 'template_gtjys6l', e.target, 'user_ZKRQ2M4aWbcCtN8SLQxJd')
    //       .then((result) => {
    //         console.log(result.text);
    //     }, (error) => {
    //         console.log(error.text);
    //     });
    //     e.target.reset()
    // }

    const signInWithGoogle = () => {
        auth
            .signInWithPopup(provider)
            .then(result => {
                console.log(result)
                dispatch({
                    type: actionTypes.SET_USER,
                    user: result.user,
                })
            }).then(() => {
                history.push('/')
            })
            .catch(error => {
                alert(error.message)
            })
    }

    const signIn = (e) => {
        // e.preventDefault();
        auth
            .signInWithEmailAndPassword(email, password)
            .then(() => {
                history.push('/')
            })
            .catch(error => (error.message))
    }

    // const verifyEmail = e => {
    //     e.preventDefault();
    //     auth.sendEmailVerification(email)
    //     .then(() => {
    //         console.log('Email sent')
    //     })
    //     .catch((error) => {
    //         alert(error.message)
    //     })
    // }

    const useStyles = makeStyles((theme) => ({
        inputField: {
            width: "100%",
            margin: theme.spacing(1, 0),
        },
    }));
    
    const classes = useStyles();
    const { register, handleSubmit, errors } = useForm();

    return (
        <div className='login'> 
            {/* <div class="area" >
                <ul class="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div> */}

            <div className="login_inner">
            <Link to='/'>
                <img
                    className='login__logo'
                    src=''
                    alt=''
                />
            </Link>
            
            <div className="login__brand">
                
                <img className="login__image" src="https://i.imgur.com/jnUWWgI.png" alt=""></img>
            </div>
            <div className='login__container'>
            
                <h1>Sign-in</h1>
                <hr />
                <form onSubmit={handleSubmit(signIn)}>
                    
                    <TextField 
                        label="Email" 
                        variant="outlined" 
                        fullwidth 
                        name="email"
                        className={classes.inputField}
                        inputRef={register({
                            required: "Email is required.",
                        })}
                        error={Boolean(errors.email)}
                        helperText={errors.email?.message}
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                    />
                
                    <TextField 
                        type='password'
                        label="Password"
                        variant="outlined"
                        fullwidth 
                        name="password"
                        className={classes.inputField}
                        inputRef={register({
                            required: "Password is required.",
                        })}
                        error={Boolean(errors.email)}
                        helperText={errors.password?.message}
                        value={password} 
                        onChange={e => setPassword(e.target.value)} 
                    />

                    <Button type='submit'>Sign In</Button>
                </form>

                <h5><span>or</span></h5>
                
                <Button onClick={signInWithGoogle}>Continue with Google</Button>
                <p>Not registered yet? <Link onClick={() => history.push('./signup')} style={{ textDecoration: 'none', color: 'black' }}>Sign-up</Link></p>
            </div>
            <div className="close__icon"><Link onClick={() => history.push('./')} style={{ textDecoration: 'none', color: 'white' }}><CloseIcon /></Link></div>
            </div>
        </div>
    );
};
export default Login;


