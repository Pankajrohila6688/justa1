import React from 'react';
import './Banner.css';
import SearchIcon from "@material-ui/icons/Search";
import LocationOnIcon from '@material-ui/icons/LocationOn';

const Banner = () => {
    return (
        <div className="banner">
            <img className="banner__image" src='https://i.imgur.com/SLehsV2.jpg' alt=""/>
            <div className="banner__box">
                
                <div class="banner__wrapper">
                    <div class="banner__words">
                        <span>Create</span>
                        <span>Register</span>
                        <span>Advertise</span>
                        <span>Boost 10X</span>
                        <span>Create</span>
                    </div>
                    <p>Your Business</p>
                </div>
                <h3 className="banner__text">
                    Just A1 is a personal companion that helps bussiness from
                     small e-commerce shops to big online retailers.
                    Build your own brand, find the right audience and advertise yourself.
                </h3>
                <div className="banner__searchBar">
                    <div className="banner__searchInput">
                        <button className="banner__buttonDesign">
                            <LocationOnIcon>
                            </LocationOnIcon>
                            <h2 className="banner__location">Where</h2>
                        </button>
                        <div className="banner__search">
                            <input type="text" placeholder="search anything" />
                            <SearchIcon className="banner__searchIcon"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Banner;