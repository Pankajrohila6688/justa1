import React, { useState, useEffect } from "react";
import "./Body.css";
import Section1 from "./Section1";
import Ads from "./Ads";
import Sponsers from "./Sponsers";
import Plans from "./Plans";
import Work from "./Work";
import Ourclient from "./Ourclient";
import Footer from "./Footer"
// import { Anchor } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { Button } from '@material-ui/core'; 
import { useStateValue } from './StateProvider';
import { auth } from './firebase';
// import Search from '../components/catagories pages/catagories components/Search';
import SearchIcon from "@material-ui/icons/Search";
import LocationOnIcon from '@material-ui/icons/LocationOn';


const Body = () => {
    const [{ user }] = useStateValue();
    const history = useHistory();
    const [navbar, setNavbar] = useState(false);
    const [show, handleShow] = useState(false);
    const handleAuthentication = () => {
        if (user) {
            alert('Press ok to Sign Out JustA1')
            auth.signOut();
        }
    }

    const listService = () => {
        alert('You have to be signed in');
        history.push('./signup1');
    }

    useEffect(() => {
        window.addEventListener("scroll", () => {
            if(window.scrollY > 350) {
                handleShow(true);
            } else handleShow(false);
        });
        setNavbar(false)
    }, [])


    return(
        
        <div className="body">
            
                <nav className={navbar ? 'navbar active' : 'navbar'}>
                    <div className="header__logo">
                        <Link to='/'>
                            <img className="header__logo" src="https://i.imgur.com/jnUWWgI.png" alt=""/>
                        </Link>
                    </div>
                    
                    <div className={`headerSearch ${show && "headerSearch__active"}`}>
                        <div className="header_searchBar">
                        <div className="header__searchInput">
                            <button className="header__buttonDesign">
                                <LocationOnIcon>
                                </LocationOnIcon>
                                <h2 className="header__location">Where</h2>
                            </button>
                            <div className="header__search">
                                <input type="text" placeholder="search anything" />
                                <SearchIcon className="header__searchIcon"/>
                            </div>
                        </div>
                    </div>
                    </div>
                  
                    
                    <div className="header__menu">
                    {user ? [<Button onClick={() => history.push('./freeplanForm')}>List Services</Button>,<Button onClick={() => history.push('./dash')}>Dashboard</Button>, <Button onClick={handleAuthentication}>Sign Out</Button>] : [<Button onClick={listService}>List Services</Button>,<Button onClick={(e) => history.push("/login")}>Login</Button>, <Button onClick={(e) => history.push("/signup")}>Sign Up</Button>]}
                    </div>
                </nav>
        
                       
            <Section1 />
            <Sponsers />
            <Ads />
            <Ourclient />
            <Work id ="work"/>
            <Plans />
            <Footer />
           
        </div>
    );
} 
export default Body;


// font-family: axiforma-regular,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica,Arial,sans-serif;
