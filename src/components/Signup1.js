import React, {useState} from 'react';
import './Login.css';
import { Button, TextField  } from '@material-ui/core';
import { auth, provider } from './firebase';
import { Link, useHistory } from 'react-router-dom';
import { useStateValue } from './StateProvider';
import { actionTypes } from './reducer';
import { useForm } from "react-hook-form";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from '@material-ui/icons/Close';

const Signup1 = () => {

    const history = useHistory()
    const [{}, dispatch] = useStateValue();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const signInWithGoogle = () => {
        auth
            .signInWithPopup(provider)
            .then(result => {
                console.log(result)
                dispatch({
                    type: actionTypes.SET_USER,
                    user: result.user,
                })
            }).then(() => {
                history.push('./freeplanForm')
            })
            .catch(error => {
                alert(error.message)
            })
    }
   
    const signUp = e => {
        // e.preventDefault();
        auth
            .createUserWithEmailAndPassword(email, password)
            .then((auth) => {
                console.log(auth);
                if (auth) {
                    history.push('./freeplanForm')
                }
            })
            .catch(error => alert(error.message))
    }
    
    const useStyles = makeStyles((theme) => ({
        inputField: {
            width: "100%",
            margin: theme.spacing(1, 0),
        },
    }));
    
    const classes = useStyles();
    const { register, handleSubmit, errors } = useForm();

    return (
        <div className='login'> 
            {/* <div class="area" >
                <ul class="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div> */}

            <div className="login_inner">
            <Link to='/'>
                <img
                    className='login__logo'
                    src=''
                    alt=''
                />
            </Link>
            <div className="login__brand">
                <img className="login__image" src="https://i.imgur.com/jnUWWgI.png" alt=""></img>
            </div>
            <div className='login__container'>
                <h1>Sign-up</h1>
                <div><hr /></div>
                <form onSubmit={handleSubmit(signUp)}>
                    {/* <h5>E-mail</h5>
                    <input type='email' value={email} onChange={e => setEmail(e.target.value)} name='email' />
                    <h5>Password</h5>
                    <input type='password' value={password} onChange={e => setPassword(e.target.value)} /> */}

                    <TextField 
                        label="Name" 
                        variant="outlined" 
                        fullwidth 
                        name="name"
                        className={classes.inputField}
                        inputRef={register({
                            required: "Name is required.",
                        })}
                        error={Boolean(errors.name)}
                        helperText={errors.name?.message}
                        value={name} 
                        onChange={(e) => setName(e.target.value)}
                    />

                    <TextField 
                        label="Email" 
                        variant="outlined" 
                        fullwidth 
                        name="email"
                        className={classes.inputField}
                        inputRef={register({
                            required: "Email is required.",
                        })}
                        error={Boolean(errors.email)}
                        helperText={errors.email?.message}
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                    />
                
                    <TextField 
                        type='password'
                        label="Password"
                        variant="outlined"
                        fullwidth 
                        name="password"
                        className={classes.inputField}
                        inputRef={register({
                            required: "Password is required.",
                        })}
                        error={Boolean(errors.password)}
                        helperText={errors.password?.message}
                        value={password} 
                        onChange={e => setPassword(e.target.value)} 
                    />

                    {/* <p>
                        By signing-in you agree to the JustA1
                        Conditions of Use & Scale. Please see our
                        Privacy Notice, our Cookies Notice
                        and our Interest-Based Ads Notice.
                    </p> */}

                    <Button onClick={signUp}>Create an Account</Button>
                </form>
                
                <h5><span>or</span></h5>

                <Button onClick={signInWithGoogle}>Continue with Google</Button>

                <p>Already have an Account? <Link onClick={() => history.push('./login1')} style={{ textDecoration: 'none', color: 'black' }}>Sign-In</Link></p>
            </div>
            <div className="close__icon"><Link onClick={() => history.push('./')} style={{ textDecoration: 'none', color: 'white' }}><CloseIcon /></Link></div>
            </div>
        </div>
    );
};

export default Signup1;
