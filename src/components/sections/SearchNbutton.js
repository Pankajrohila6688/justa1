import React from 'react';
import "./SearchNbutton.css"
import SearchIcon from '@material-ui/icons/Search';
import { Button } from '@material-ui/core';

const SearchNbutton = () => {
    return (
        <div className="searchnbutton">
            <div className='searchnbutton__content'>
                <Button variant='outlined' color='primary'>Select Location</Button>
            </div>
            <div className='searchnbutton__search'>
                <input type='text' placeholder='search anything' />
                <SearchIcon />
            </div>
        </div>
    )
}

export default SearchNbutton;
