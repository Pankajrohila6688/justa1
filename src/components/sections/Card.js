import React from "react";
import './Card.css';
import { useHistory } from 'react-router-dom';

const Card = ({src, title, type, description, onClick, id}) => {
    return(
        <div onClick={onClick} className="card">
            <img src={src} alt=""></img>
            <div className="card__info">
                <h2>{title}</h2>
                <h3>{type}</h3>
                <h3>{description}</h3>   
            </div>
        </div>
    );
}

export default Card;