import React from "react";
import './Card.css';
import { useHistory } from 'react-router-dom';
import { db } from '../firebase';
import firebase from 'firebase';
import { useStateValue } from '../StateProvider';

const PopularCard = ({src, title, type, description, onClick, id}) => {
    const history = useHistory();
    const [{ basket, user }, dispatch] = useStateValue();

    const selectCard = async () => {
        if (id) {
            const increment = firebase.firestore.FieldValue.increment(1);
            history.push(`/productService/${id}`)
            // const val = db.collection("userform").doc(id);
            // val.update({ 'views': increment })
            const snapshot =  await db.collection("userform").where("id", "==", id).where("uid", "!=", user?.uid).get();
            const promises = [];
            snapshot.forEach((doc) => {
                promises.push(doc.ref.update({
                    'views': increment
                }))
            })
            await Promise.all(promises);

        }
        else {
            history.push("/")
        }
    }
    return(
        <div onClick={selectCard} className="card">
            <img src={src} alt=""></img>
            <div className="card__info">
                <h2>{title}</h2>
                <h3>{type}</h3>   
            </div>
        </div>
    );
}

export default PopularCard;