import { React, useState, useEffect } from 'react';
import './Ads.css';
import PopularCard from './sections/PopularCard';
import  { db } from "./firebase";
// import { Button } from '@material-ui/core';

const Ads = ({ src, name, type, area, landmark }) => {
    const [details, setDetails] = useState([]);
    useEffect(() =>{
        db.collection('userform').orderBy('timestamp','desc').limit(12).onSnapshot(snapshot => (
            setDetails(snapshot.docs.map(doc => ({
                id: doc.id,
                name: doc.data()
            })))
        ))
    }, [])
    return (
        <div className="ads">
            <div className="ads__showcaseContainer">
                <div className="ads__showcase">
                    <img src="https://i.imgur.com/rTx7mET.jpg" alt=""></img>
                </div>
                <div className="ads__showcase">
                    <img src="https://i.imgur.com/rTx7mET.jpg" alt=""></img>
                </div>

                <div className="ads__showcase">
                    <img src="https://i.imgur.com/rTx7mET.jpg" alt=""></img>
                </div>
                <div className="ads__showcase">
                    <img src="https://i.imgur.com/rTx7mET.jpg" alt=""></img>
                </div>
                <div className="ads__showcase">
                    <img src="https://i.imgur.com/rTx7mET.jpg" alt=""></img>
                </div>
                <div className="ads__showcase">
                    <img src="https://i.imgur.com/rTx7mET.jpg" alt=""></img>
                </div>
                <div className="ads__showcase">
                    <img src="https://i.imgur.com/rTx7mET.jpg" alt=""></img>
                </div>
                
                
                
            </div>    
            <div className="ads__list">
                <h2>Popular Services</h2>
                <hr></hr>
 
                <div className="ads__cards">
                {
                        details && details.map(detail => {
                            return (
                                <PopularCard
                                    src={detail.name.singleImageUrl}
                                    title={detail.name.businessName}
                                    type={detail.name.businessType}
                                    id={detail.id}  
                                />
                            )
                        })
                }
                </div>
            </div>
        </div>
    );
};
export default Ads;