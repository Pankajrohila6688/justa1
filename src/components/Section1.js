import React from "react";
import './Section1.css';
import Services from "./Services";
import SubCatagories from "./catagories pages/catagories components/SubCatagories";
const Section1 = () => {
    return(
        <div className="Section1">
            <h2>Popular Categories</h2>            
            <hr />    
            
            
            
            <div className="Section1__container">
                <div className="services1"><Services /></div>
                <div className="subs">
                    <div className="Section1__head">
                        <h2>Browse Catagories</h2>
                        <hr />
                    </div> 
                    <SubCatagories />
                </div>
            </div>  
        </div>
    );
}
export default Section1;