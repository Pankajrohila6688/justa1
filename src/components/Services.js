import React from 'react';
import Card from './sections/Card';
import  './Services.css';
import { useHistory } from "react-router-dom";
const Services = () => {
    const history = useHistory();
    return (
        <div className='services'>
                    <div className='services__section'>
                        <Card
                            onClick={() => history.push('./travel')}
                            src="https://i.imgur.com/NNb1X6O.jpg"
                            title='TRAVEL'
                            description='Travel the world'
                        />
                        <Card
                            onClick={() => history.push('./automotive')}
                            src='https://i.imgur.com/u2apV9B.jpg'
                            title='AUTOMOTIVE'
                            description='Travel the world'
                        />  
                        <Card
                            onClick={() => history.push('./medical')}
                            src='https://i.imgur.com/T9RNYr9.jpg'
                            title='MEDICAL'
                            description='Feel the luxury'
                        />
                        <Card
                            onClick={() => history.push('./education')}
                            src='https://i.imgur.com/PYbFwZ0.jpg'
                            title='EDUCATION'
                            description='Feel the luxury'
                        />
                        <Card
                            onClick={() => history.push('./repairs')}
                            src='https://i.imgur.com/wIiqWlC.jpg'
                            title='REPAIRS'
                            description='Repairs'
                        />
                        <Card
                            onClick={() => history.push('./shopping')}
                            src='https://i.imgur.com/br4sxeN.jpg'
                            title='SHOPPING'
                            description='Feel the luxury'
                        />
                        <Card
                            onClick={() => history.push('./personalcare')}
                            src='https://i.imgur.com/6vPtd3y.jpg'
                            title='PERSONAL CARE'
                            description='Feel the luxury'
                        />
                        <Card
                            onClick={() => history.push('./restaurant')}
                            src='https://i.imgur.com/GGceF5y.jpg'
                            title='RESTAURANT'
                            description='Feel the luxury'
                        />
                    </div>
        </div>
    );
}
export default Services;