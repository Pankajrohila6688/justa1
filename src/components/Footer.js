import React from "react";
import "./Footer.css"
import TwitterIcon from '@material-ui/icons/Twitter';
import FacebookIcon from '@material-ui/icons/Facebook';
import MailIcon from '@material-ui/icons/Mail';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import InstagramIcon from '@material-ui/icons/Instagram';
// import { Anchor } from 'antd';
// const { Link } = Anchor;
const Footer = () => {
    return (
        <div className="footer">  

            <img className="footer__logo" src="https://i.imgur.com/jnUWWgI.png" alt=""></img>
                    
            <div className="footer__content">
                <div key="work" className="footer__contentDetails">
                    <ul className="footer__contentHead">Why JustA1</ul>
                    <ul>Ability</ul>
                    <ul>UserExperience</ul>
                    <ul>Boost Productivity</ul>
                </div>    
               
                <div key="" className="footer__contentDetails">
                    <ul className="footer__contentHead">Featured Catagories</ul>
                        <ul>Travel</ul>
                        <ul>Restaurant</ul>
                        <ul>Repair</ul>
                        <ul>Automobile</ul>
                        <ul>Health</ul>
                        <ul>Fitness</ul>
                        <ul>Shopping</ul>   
                </div>
                <div key="" className="footer__contentDetails">
                    <ul className="footer__contentHead">Plans</ul>
                    <ul>Free Plan</ul>
                    <ul></ul>
                </div>
                <div className="footer__contentDetails">
                    <ul className="footer__contentHead">Popular Services</ul>
                    <ul>Advertsed Services</ul>
                    <ul>Service</ul>
                    <ul></ul>
                </div>
            </div>
            <hr />
            <div key="" className="footer__Bottom">
                <div className="footer__BottomLeft">
                    <p>Privacy & Terms</p>
                    <p>Contact Us</p>
                </div>
                <div className="footer__BottomRight">
                    <TwitterIcon />
                    <FacebookIcon />
                    <MailIcon />
                    <LinkedInIcon />
                    <InstagramIcon />
                </div>
            </div>
            <div className="footer__Copyright">CopyrighText</div>
        </div>

        
    )
}
export default Footer;