import React, { useState } from 'react';
import './FreeplanForm.css';
import firebase from "firebase";
import { db } from '../firebase';
import { storage } from '../firebase';
import { Link, useHistory } from 'react-router-dom';
import Header from '../Header'; 
import Footer from "../Footer";
import {
    Input,
    Select,
    MenuItem,
    Button,
    FormControl,
    FormHelperText,
    InputLabel,
    FormControlLabel,
    Checkbox,
    FormLabel,
    RadioGroup,
    Radio,
    Switch,
    Divider,
} from "@material-ui/core";

import { useForm, Controller } from "react-hook-form";
import { makeStyles } from "@material-ui/core/styles";
import { Multiselect } from "multiselect-react-dropdown";

import emailjs from 'emailjs-com';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import CancelIcon from '@material-ui/icons/Cancel';
// import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { useStateValue } from '../StateProvider';
import CloseIcon from '@material-ui/icons/Close';

const FreeplanForm = () => {

    const data = [
        {Day: "Monday", id: 1},
        {Day: "Tuesday", id: 2},
        {Day: "Wednesday", id: 3},
        {Day: "Thursday", id: 4},
        {Day: "Friday", id: 5},
        {Day: "Saturday", id: 6},
        {Day: "Sunday", id: 7},
    ]

    
    const history = useHistory();
    const [{ basket, user }, dispatch] = useStateValue();

    const [days, setDays] = useState(data);
    const [am, setAm] = useState("");
    const [pm, setPm] = useState("");
    
    const [questions, setQuestions] = useState([{ value: null }]);
    const [answers, setAnswers] = useState([{ value: null }]);

    const [businessName, setBusinessName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [tagline, setTagline] = useState("");
    const [city, setCity] = useState("");
    const [website, setWebsite] = useState("");
    const [businessType, setBusinessType] = useState("");
    const [businessDescription, setBusinessDescription] = useState("");
    // const [services, setServices] = useState("");
    
    const [images, setImages] = useState([]);
    const [urls, setUrls] = useState([]);

    const [image, setImage] = useState(null);
    const [url1, setUrl1] = useState([]);

    const [progress, setProgress] = useState(0);

    let views = 0;
    let leads = 0;

    const handleChangeImage = (e) => {
        if (e.target.files[0]) {
          setImage(e.target.files[0]);
        }
    };

    const handleChange = (e) => {
        for (let i = 0; i < e.target.files.length; i++) {
            const newFile = e.target.files[i];
            newFile["id"] = Math.random();
          // add an "id" property to each File object
            setImages(prevState => [...prevState, newFile]);
            console.log(newFile)
        }
    };

    function handleChange1(i, event) {
        const values = [...questions];
        values[i].value = event.target.value;
        setQuestions(values);
    }

    function handleChange2(i, event) {
        const values = [...answers];
        values[i].value = event.target.value;
        setAnswers(values);
    }

    function handleAdd() {
        const values1 = [...questions];
        const values2 = [...answers];
        values1.push({ value: null });
        values2.push({ value: null });
        setQuestions(values1);
        setAnswers(values2);
    }

    function handleRemove(i) {
        const values1 = [...questions];
        const values2 = [...answers];
        values1.splice(i, 1);
        values2.splice(i, 1);
        setQuestions(values1);
        setAnswers(values2);
    }

    const singleImage = () => {
        const promise = [];

        const uploadTask = storage.ref(`images/${image.name}`).put(image);
        uploadTask.on(
            "state_changed",
            (snapshot) => {
              // progress function ...
              const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setProgress(progress);
            },
            (error) => {
              // Error function ...
              console.log(error);
            },
            () => {
              // complete function ...
              storage
                .ref("images")
                .child(image.name)
                .getDownloadURL()
                .then((url) => {
                  setUrl1(url);
                  url1.push(url);
                });
            }
        )
        Promise.all(promise);
    }
    
    const delay = ms => new Promise(res => setTimeout(res, ms));
    const yourFunction = async () => {
        await delay(5000);
        console.log("Waited 5s");
    
        await delay(2000);
        console.log("Waited an additional 3s");
    };

    const sendEmail = async (e) => {

        await onFormSubmit();

        e.preventDefault();    //This is important, i'm not sure why, but the email won't send without it
        

        // emailjs.sendForm('service_2mvsn7k', 'template_xavu779', e.target, 'user_ZKRQ2M4aWbcCtN8SLQxJd')
        //   .then((result) => {
        //     console.log(result.text); //  window.location.reload()  //This is if you still want the page to reload (since e.preventDefault() cancelled that behavior) 
        //   }, (error) => {
        //       console.log(error.text);
        //   });
        console.log('Email sent')
    }

    const onFormSubmit = async (e) => {

        history.push("./dashBusiness")
        // e.preventDefault(); // prevent page refreshing
        // const urlArray = [];
        const promises = [];
        
        await singleImage()

        await images.forEach(image => {
        const uploadTask1 = 
        firebase.storage().ref().child(`images/${image.name}`).put(image);
        promises.push(uploadTask1);
        uploadTask1.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            snapshot => {
            const progress = 
                ((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                if (snapshot.state === firebase.storage.TaskState.RUNNING) {
                console.log(`Progress: ${progress}%`);
                setProgress(progress);
                }
            },
            error => console.log(error.code),
            
            async () => {
                const downloadURL = await uploadTask1.snapshot.ref.getDownloadURL();
                // do something with the url
                console.log(downloadURL);
                setUrls(downloadURL);
                urls.push(downloadURL);
                console.log(urls);
                // yourFunction();
            }
            
        )
        })
        

        Promise.all(promises)
        yourFunction()
        .then(() => {
        // yourFunction();
        const docRef = db.collection("userform").doc();
        docRef.set({
            imageUrl: urls,
            singleImageUrl: url1,
            businessName: businessName,
            mobileNo: mobileNo,
            email: email,
            city: city,
            address: address,
            tagline: tagline,
            questions: questions,
            answers: answers,
            website: website,
            am: am,
            pm: pm,
            days: days,
            businessType: businessType,
            businessDescription: businessDescription,
            uid: user.uid,
            id: docRef.id,
            views: views,
            leads: leads,
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            });
        setBusinessName("")
        setTagline("")
        setAddress("")
        setMobileNo("")
        setCity("")
        setQuestions("")
        setAnswers("")
        setWebsite("")
        // setDays("")
        setBusinessType("")
        setAm("")
        setPm("")
        setEmail("")
        setBusinessDescription("")
        })
        .catch(err => console.log(err.code))
        .then(() => alert('Your form has been submitted'))
        .then(() => history.push('./dashBusiness'))
    }

  
    const useStyles = makeStyles((theme) => ({
        // inputField1: {
        //     width: "50%",
        //     // position: "absolute",
        //     margin: theme.spacing(1, 0),
    }));
    
    const classes = useStyles();
    const { register, handleSubmit, control, errors } = useForm();

    return (
        <div className="freePlanForm">
            <Header />
            <h1 className="freePlanHeader">
                List Your Business
            </h1>
            <hr />

            {/* <h2>List your business for FREE with India's leading business platform</h2> */}

            <form className="form" onSubmit={sendEmail}>

                <div className="form__div1">
                    <br />
                    <h1>Primary listing Details</h1>
                    <hr className="form__hr"/>
                    <br />
                    <h3>Business Name *</h3>
                    <input 
                        // required
                        placeholder="Staple & Fancy Hotel" 
                        variant="outlined" 
                        name="businessName"
                        value={businessName} 
                        onChange={(e) => setBusinessName(e.target.value)} 
                    />

                    <h3>Tagline</h3>
                    <input 
                        // required
                        placeholder="Tagline Example: Best Express Mexican Grill" 
                        variant="outlined" 
                        name="tagline"
                        value={tagline} 
                        onChange={(e) => setTagline(e.target.value)}
                    /> 

                    <h3>Address</h3>
                    <input 
                        // required
                        placeholder="Enter your Address" 
                        variant="outlined" 
                        name="address"
                        value={address} 
                        onChange={(e) => setAddress(e.target.value)}
                    />    

                    <h3>City</h3>
                    <input 
                        // required
                        placeholder="Select Your Listing Region" 
                        variant="outlined" 
                        name="city"
                        value={city} 
                        onChange={(e) => setCity(e.target.value)} 
                    />

                    <h3>Website</h3>
                    <input 
                        // required
                        placeholder="Enter Your website url Example: http://website.com" 
                        variant="outlined" 
                        name="website"
                        value={website} 
                        onChange={(e) => setWebsite(e.target.value)}
                    />

                    <h3>Phone Number</h3>
                    <input 
                        // required
                        placeholder="111-111-1234" 
                        variant="outlined" 
                        name="phoneNumber"
                        value={mobileNo} 
                        onChange={(e) => setMobileNo(e.target.value)} 
                    />
                </div>
                
                <div className="form__div2">
                    <br />
                    <h3>Catagory & Services</h3>
                    <hr className="form__hr"/>
                    <br />

                    <h3> Category *</h3>
                    <select classsName="select" required name="businessType" value={businessType} onChange={(e) => setBusinessType(e.target.value)}>
                        <option disabled>Choose Your Business Catagory</option>
                        <option hidden>Select</option>
                        <option value="Travel">Travel</option>
                        <option value="Groceries">Groceries</option>
                        <option value="Wedding">Wedding</option>
                        <option value="BabyCare">Baby Care</option>
                        <option value="Bills And Recharge">Bills And Recharge</option>
                        <option value="Hotels">Hotels</option>
                        <option value="Cinema">Cinema</option>
                        <option value="Tatto">Tatto</option>
                        <option value="Beauty">Beauty</option>
                        <option value="Security">Security</option>
                        <option value="Pet Care">Pet Care</option>
                        <option value="Packers And Movers">Packers And Movers</option>
                        <option value="Internet Services">Internet Services</option>
                        <option value="House Keeping">House Keeping</option>
                        <option value="Insuarance">Insuarance</option>
                        <option value="Home Decor">Home Decor</option>
                        <option value="Fitness">Fitness</option>
                        <option value="Music And Dance">Music And Dance</option>
                        <option value="Advertising Companies">Advertising Companies</option>
                        <option value="Sports">Sports</option>
                        <option value="Automotive">Automotive</option>
                        <option value="Restaurant">Restaurant</option>
                        <option value="Education">Education</option>
                        <option value="Repairs">Repairs</option>
                        <option value="Personal Care">Personal Care</option>
                        <option value="Medical">Medical</option>
                        <option value="Shopping">Shopping</option>
                    </select>
                </div>
                
                <div className="form__div3">
                    <br />
                    <h3>Business Hours</h3>
                    <hr className="form__hr"/>
                    
                    <h3> Days & Timings *</h3>
                    <br />
                    {/* <Multiselect options={days} showCheckbox displayValue="Day" showArrow value={days} onChange={(e) => setDays(e.target.value)}/> */}
                    <FormControl >
                        {/* <InputLabel required control={control} defaultValue="">
                        </InputLabel> */}
                        <FormHelperText>* Please select your working days, you can choose multiple days</FormHelperText>
                        <Select className="meterialSelect" required multiple name="days" value={days} onChange={(e) => setDays(e.target.value)}>
                            <MenuItem value="" disabled>Click to select multiple days or single, click again to deselect</MenuItem>
                            <MenuItem className="menuItem" value="Monday">Monday</MenuItem>
                            <MenuItem value="Tuesday">Tuesday</MenuItem>
                            <MenuItem value="Wednesday">Wednesday</MenuItem>
                            <MenuItem value="Thursday">Thursday</MenuItem>
                            <MenuItem value="Friday">Friday</MenuItem>
                            <MenuItem value="Saturday">Saturday</MenuItem>
                            <MenuItem value="Sunday">Sunday</MenuItem>
                        </Select>
                            
                    </FormControl>
                    
                    <div className="form__div3insideDiv">
                        <select classsName="select" required value={am} onChange={(e) => setAm(e.target.value)}>
                            <option>AM</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>
                        </select>

                        <h3>To</h3>

                        <select classsName="select" required value={pm} onChange={(e) => setPm(e.target.value)}>
                            <option>PM</option>
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                    </div>

                </div>

                <div className="form__div4">
                    <br />
                    <h3>Social Media</h3>
                    <hr className="form__hr"/>
                    <br />
                </div>

                <div className="form__div5">
                    <br />
                    <h3>Frequently Asked Questions</h3>
                    <hr className="form__hr"/>
 
                    <button type="button" className="addBtn" onClick={() => handleAdd()}>+ Add New</button>
                    {(questions,answers).map((field, idx) => {
                        return (
                        <div key={`${field}-${idx}`} className="form__div5">
                            <button type="button" className="removeBtn"onClick={() => handleRemove(idx)}><CloseIcon /></button>
                            <input 
                                className="input1"
                                // required
                                placeholder="Frequently Asked Questions" 
                                variant="outlined" 
                                name="question"
                                onChange={e => handleChange1(idx, e)}
                            />
                            <textarea 
                                className="input2"
                                // required
                                placeholder="Answer" 
                                variant="outlined" 
                                name="answer"
                                onChange={e => handleChange2(idx, e)}
                            />
                            {/* <button type="button" onClick={() => handleAdd()}>+</button> */}
                            {/* <button type="button" onClick={() => handleRemove(idx)}><CloseIcon /></button> */}
                        </div>
                        );
                    })}
                    
                </div>

                <div className="form__div6">
                    <br />
                    <h3>More Info</h3>
                    <hr className="form__hr"/>
                    <br />

                    <h3>Business Description</h3>
                    <textarea 
                        // required
                        placeholder="Enter your business description"
                        label="Business Description" 
                        variant="outlined" 
                        multiline="true"
                        rowsMax="Infinity"
                        name="businessDescription"
                        value={businessDescription} 
                        onChange={(e) => setBusinessDescription(e.target.value)} 
                    />
                </div>

                <div className="form__div7">
                    <br />
                    <h3>Media</h3>
                    <hr className="form__hr"/>
                    <br />

                    <h3>Business Banner Image (Single)</h3>
                    <label className="form__image">
                        <input className="custom-file-input" type="file" onChange={handleChangeImage}/>
                        <AddAPhotoIcon className="icon"/> 
                        <p>Add Photo</p>
                    </label>
                    <br />

                    <h3>Business Featured Images (Multiple)</h3>
                    <label className="form__image">
                        <input className="custom-file-input" type="file" onChange={handleChange} multiple/>
                        <AddAPhotoIcon className="icon"/> 
                        <p>Add Photo</p>
                    </label>

                    
                </div>

                <div className="form__div8">
                    <br />
                    <h3>Contact Email</h3>
                    <hr className="form__hr"/>
                    <br />

                    <h3>Email</h3>
                    <input 
                        // required
                        placeholder="Your contact email" 
                        variant="outlined" 
                        name="email"
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>
                    
                <div className="form__div9">
                    <br />
                    <h3>Submit Your Form</h3>
                    <hr className="form__hr"/>
                    <br />
                    
                    <h3>Proceed to submit your form</h3>

                    <progress className="imageupload__progress" value={progress} max="100" />
                    <progress className="imageupload__progress" value={progress} max="100" />

                    <Button type="submit">Submit</Button> 
                </div>
            
            </form>
            <Footer/>
        </div>
    );
};

export default FreeplanForm;