// import React from 'react';
// import './ListingForm.css';
// import { useHistory } from 'react-router-dom';

// const ListingForm = () => {

//     const history = useHistory();

//     return (
//         <div className="listingform">
//             <button onClick={() => history.push('./freeplanForm')}>Free plan</button>
//             <button>Paid Plan</button>
//         </div>
//     );
// };

// export default ListingForm;

import React from 'react';
import './ListingForm.css';
import { useHistory } from 'react-router-dom';
import { Button } from '@material-ui/core'

const ListingForm = () => {
    const history = useHistory();
    return (
        <div className='listingform'>
            <div className="listingform__header">
                <h2>
                    Choose the plan that suits you !
                </h2>
            </div>
            <div className='login'> 
                <div class="area" >
                    <ul class="circles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>    
            <div className="listingform__box">
                <div className="listingform__sectionOne">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li><Button onClick={() => history.push('./freeplanForm')}>Free plan</Button></li>
                    </ul>
                    
                </div>
                <div className="listingform__sectionTwo">
                <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li><Button>Paid Plan</Button></li>
                    </ul>
                </div>
            </div>
        </div>
    );
};
export default ListingForm;