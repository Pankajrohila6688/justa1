import React, { useState } from 'react';
import './Header.css';
import { Link, useHistory } from 'react-router-dom';
import { Button } from '@material-ui/core'; 
import { useStateValue } from './StateProvider';
import { auth } from './firebase';
// import Search from '../components/catagories pages/catagories components/Search';
import SearchIcon from "@material-ui/icons/Search";
import LocationOnIcon from '@material-ui/icons/LocationOn';
const Header = () => {

    const [{ user }] = useStateValue();
    const history = useHistory();
    const [navbar, setNavbar] = useState(false);

    const handleAuthentication = () => {
        if (user) {
            alert('Press ok to Sign Out JustA1')
            auth.signOut();
        }
    }

    // const listService = () => {
    //     alert('You have to be signed in');
    //     history.push('./signup1');
    // }

    const changeBackground = () => {
        if(window.scrollY >= 60) {
            setNavbar(true);
        }
        else {
            setNavbar(false);
        }
    }
    window.addEventListener('scroll', changeBackground);  

    return(
        <div className = "header">
            <nav className={navbar ? 'navbar active' : 'navbar'}>
                    <div className="header__logo">
                        <Link to='/'>
                            <img className="header__logo" src="https://i.imgur.com/jnUWWgI.png" alt=""/>
                        </Link>
                    </div>
                        <div className="header_searchBar">
                        <div className="header__searchInput">
                            <button className="header__buttonDesign">
                                <LocationOnIcon>
                                </LocationOnIcon>
                                <h2 className="header__location">Where</h2>
                            </button>
                            <div className="header__search">
                                <input type="text" placeholder="search anything" />
                                <SearchIcon className="header__searchIcon"/>
                            </div>
                        </div>
                    </div>
                
                  
                    
                    <div className="header__menu">
                    {user ? [<Button onClick={() => history.push('../freeplanForm')}>List Services</Button>,<Button onClick={() => history.push('../dash')}>Dashboard</Button>, <Button onClick={handleAuthentication}>Sign Out</Button>] : [<Button onClick={(e) => history.push("/login")}></Button>, <Button onClick={(e) => history.push("/signup")}></Button>]}
                    </div>
                </nav>
        </div>
    );
}
export default Header;